# Report of the meeting :

[TOC]

Code and more information on our implementation here :
https://github.com/Venon282/Projet-INFO4

## First meeting : 

Around the project :
- Interview classmates from year 5 who did this project
- Write specifications

Edge impulse : 
- learn the environment
- learn DP of course 

The project :
- Test their codes on the map
- Improve accuracy on larger data sets
- Make sure to deploy it on other embedded boards

## Meeting with the fifth years :

Why did you choose this site? 
- To train a model, they found it easier

Better to use tenserflow ? 
- Certainly

Problems :
- Lack of data
- The microphones are not good (record them with the microphone in question (problem of the size of the dataset if you do that))

Avantages :
- STM or wayterminal with external microphones
- Possibility with the site to scan a qr code on tel and to use the model (better quality microphone)

## Third meeting : 
-Training, look grid5000 


# Advancement

## Session 1
- Self-training

## Session 2
- Self-training

## Session 3
- Creation of a first model for a species of birds

## Session 4
- Find and retrieve datasets

## Session 5
- Performs training with various parameters

## Session 6
- Adaptation of the model to several bird species

## Session 7
- Switching to grid5000
- Search for a possible new model

## Session 8
- Model resolution under grid5000
- Implementing a new model : RNN

## Session 9
-End up the first model
-Testing and optimizing the new model

## Session 10
-Converting the RNN model to tensor flowlite

## Session 11
-Testing the tensorflow lite model
-Starting working with embeded cards

## Session 12
-Implementing a code to record real times audio

## Session 13
-Linking the Nucleo card to the RNN model 
-Testing


